# Change Log

## [1.2.0]
- Added Boriel's ZX Basic Compiler extended syntax.
- Fixed more typos.
- Added some missing statements.

## [1.1.1]
- Add logo.

## [1.0.1]
- Fix typos.
- Add installation instructions.

## [1.0.0]
- Initial release.

